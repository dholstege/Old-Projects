# Old Projects
Contains a couple of old projects I worked on, both in school and independently. These projects are not currently in development, and are meant as an example of my work.

## Coursework
* [encryption-extension](./coursework/encryption-extension/): Chrome extension for encrypting personal files, using the experimental HTML5 filesystem API
* [shell-alignment](./coursework/shell-alignment/): Data processing scripts and alignment algorithm for mollusk shell timeline generation
* [twitch-ml](./coursework/twitch-ml/): Machine learning project for moderating a Twitch chat
* [twitch-visualization](./coursework/twitch-visualization/): 3D visualization of Twitch chat data in Maya

## Personal Projects
* [discord-helper](./personal/discord-helper/): Some basic moderation features for Discord
* [twitch-antispam](./personal/twitch-antispam/): Anti-spam bot for Twitch that tracks user activity
* [scripts](./personal/scripts/): Scripts from managing game servers
