/**
 * Created by Davis Holstege on 12/25/2015.
 * Main file for the new anti-spam bot
 * Built using tmi.js, redis, and validator
 * Before running launch the redis server from an admin command prompt by running 'redis-server --service-start'
 */

/**
 * TODO: check if the user is a mod/staff before trying to time them out. Just return if they are
 * TODO: create set up script/guide
 */

var irc = require("tmi.js");
var redis = require('redis');
var validator = require('validator');

//Anti-Spam Config
var postThreshold = 1; //If the user has made fewer posts than this, any message with a link will get timed out. Gets changed later on if there's a config set in the DB

//Bot Options Setup
var channels; //Gets loaded from the database later
var chat_options = {
    options: {
        debug: true
    },
    connection: {
        reconnect: true,
        random: 'chat'
    },
    identity: {},
    channels: []
};
var group_options = {
    options: {
        debug: true
    },
    connection: {
        reconnect: true,
        random: 'group'
    },
    identity: {},
    channels: []
};
var chat_client;
var group_client;

//Setup Database/Load Configs
var db = redis.createClient();
db.on('connect', function(){
    console.log('Connected to the database');

    db.hexists('config', 'threshold', function(err, reply){
        if (reply === 1) {
            db.hget('config', 'threshold', function (err, reply) {
                    postThreshold = reply;
            });
        }
        else {
            db.hset('config', 'threshold', postThreshold);
        }
    }); //Checks if there is a key for config.threshold in the DB, loads it if there is and sets it to hard coded default if there isn't

    db.hexists('identity', 'username', function(err, reply){
        if (reply === 1){
            db.hget('identity', 'username', function(err, reply){
                chat_options.identity.username = reply;
                group_options.identity.username = reply;

                db.hexists('identity', 'oauth', function(err, reply){
                    if (reply === 1){
                        db.hget('identity', 'oauth', function(err, reply){
                            chat_options.identity.password = reply;
                            group_options.identity.password = reply;

                            db.exists('channels', function(err, reply){
                                if (reply === 1){
                                    db.smembers('channels', function(err, reply){ //This should retrieve the list of connected channels
                                        channels = reply;
                                        console.log(reply);
                                        twitchConnect(); //Connect bots once the channels are set
                                    });
                                } //List existed so load it
                                else {
                                    db.sadd(['channels', chat_options.identity.username]); //I THINK this will create a set (no duplicates) that uses the bot's username as the only entry
                                    channels = chat_options.identity.username;
                                    twitchConnect(); //Connect bots once the channels are set
                                } //The key did not exist so we're going to make it and assign the bot's username to be our channel
                            }); //Checks if the channelList is stored in the database, creates it if not, then connects bots
                        });
                    }
                    else {
                        console.log('Please add the oauth key to the database under the key \'identity.oauth\,,');
                    }
                }); //The oauth was stored in the database, so assign it to the bots' configs then check for the channelList

            }); //Grab the username and assign it to both of the bots' configs, then check for the oauth
        }
        else {
            console.log('Please add the username to the database under the key \'identity.username\'');
        }
    }); //First check if there is a username stored in the database, then checks for oauth, then channellist, and finally connects bots
}); //Connects to the db and loads some configs from it, then calls twitchConnect() to connect the bots
function twitchConnect(){
    chat_options.channels = channels; //The list of channels pulled from the database
    group_options.channels = [group_options.identity.username]; //The group client only needs to be in one channel to handle whispers, this should put it in the bot's own channel

    chat_client = new irc.client(chat_options);
    group_client = new irc.client(group_options);

    //Connect the bots
    chat_client.connect();
    group_client.connect();

    addListeners(); //This is so that the chat_client will be defined before getting to this point
} //Function to be run once the configs have been loaded from the db, connects the bots to the twitch servers

//PUT LISTENERS INSIDE THIS FUNCTION
/**
 * addListeners
 * To avoid assigning listeners to the bots before they have been initialized put all listeners in here. This function gets called once the configs have been fully loaded
 * Called from twitchConnect()
 */
function addListeners() {
    //Chat Listeners
    chat_client.on('chat', function (channel, user, message, self) {

        var messageParts = message.split(/[ ,]+/).filter(Boolean); //Cuts the string into each word at whitespaces and commas
        var hadLink = false;

        for (i = 0; i < messageParts.length; i++) { //Run the test on each word in the message to check if it is a link
            if (validator.isURL(messageParts[i], {
                    protocols: ['http', 'https', 'ftp'],
                    require_tld: true,
                    require_protocol: false,
                    require_valid_protocol: false,
                    allow_underscores: false,
                    host_whitelist: false,
                    host_blacklist: false,
                    allow_trailing_dot: false,
                    allow_protocol_relative_urls: false
                })) {
                //Warning: this WILL detect some false positives like foo.bar or this.is.false
                hadLink = true; //Used later to decide if the post counts towards the post count or is evil
            }
        }

        var usernameChannelKey = user.username + '.' + channel; //makes a string to use as the key in the form username.channel

        db.hexists(user.username, channel, function (err, reply) { //Checks if the key exists at all and then handles logic about what to do with it
            if (reply === 1) {
                db.hget(user.username, channel, function (err, reply) {
                    if (!hadLink || reply >= postThreshold) {
                        //console.log('No link or over threshold');
                        db.hincrby(user.username, channel, 1); //Can add callback if needed
                    } //Post was ok, increment the post number
                    else if (hadLink && reply < postThreshold) {
                        console.log('Link and under threshold');
                        chat_client.timeout(channel, user.username, 1).then(function(){//Happens after a user has been purged successfully
                            console.log('Successfully purged a link');
                            group_client.whisper(user.username, 'You tried to post a link on channel: ' + channel + ' but do not have link permissions yet. Link permissions are automatically earned by participating in chat.');
                        }, function(err){ //Happens if there was an error purging a user
                            console.log(err);
                        });
                    } //This should only trigger if there was a link AND it was under the threshold
                    else {
                        console.log('ln 141: This is an error of some kind, never should happen');
                    } //Error, shouldn't ever happen
                });
            } //The key exists so check if it is above the threshold
            else {
                if (hadLink) {
                    console.log('Link and under threshold');
                    chat_client.timeout(channel, user.username, 1).then(function(){//Happens after a user has been purged successfully
                        console.log('Successfully purged a link');
                        group_client.whisper(user.username, 'You tried to post a link on channel: ' + channel + ' but do not have link permissions yet. Link permissions are automatically earned by participating in chat.');
                    }, function(err){ //Happens if there was an error purging a user
                        console.log(err);
                    });
                }
                else { //The key did not exist and there was not a link
                    db.hset(user.username, channel, 1, function (err, reply) {
                        console.log(reply); //TODO: remove, only for testing
                    });
                }
            } //The key did not exist, create the key UNLESS there was a link

        });

        if (messageParts[0] === '!points' && messageParts.length <= 2){
            var lookupUser;
            var res;

            if (messageParts.length === 2) lookupUser = messageParts[1];
            else lookupUser = user.username;

            db.hexists(user.username, channel, function(err, reply){
               if (reply === 1){
                   db.hget(user.username, channel, function(err, reply){
                       chat_client.say(channel, lookupUser + " has a score of: " + reply);
                   });
               } //User had a score for the channel, so log it
               else {
                   chat_client.say(channel, lookupUser + " has not earned any points yet.");
               }//The user did not have a score, probably shouldn't ever happen but just in case
            });
        } //If there's a parameter passed to !points check that as the username, otherwise check whoever sent the command in the first place


    }); //Checks if there was a URL in any message and does spam detection. Also handles in-chat commands

    //Whisper Listeners
    group_client.on('whisper', function(username, message){
        if (isAdmin(username)){
            var messageParts = message.split(/[ ,]+/).filter(Boolean); //Cuts the string into each word at whitespaces and commas
            if (message.indexOf('!threshold') === 0){
                if (!isFinite(messageParts[1])){
                    group_client.whisper(username, messageParts[1] + ' is not a valid integer');
                } //The first parameter was not an integer
                else {
                    setThreshold(+messageParts[1]); //the + theoretically will convert from a string into a number
                    group_client.whisper(username, 'Successfully set threshold to ' + messageParts[1]);
                }
            } //Sets the threshold to the first thing after "!threshold" if given a valid int
            else if (message.indexOf('!join') === 0){
                chat_client.join(messageParts[1]).then(function(){//Joined channel successfully, now update the DB and channelList var
                    channels.push(messageParts[1]);
                    db.sadd('channelList', messageParts[1]); //Add the channel to the database's channelList key so that it loads next time too
                    group_client.whisper(username,'The bot has joined, or was already in, channel ' + messageParts[1]);
                }, function(err){ //There was an error joining the channel TODO: whisper whoever triggered the join to alert them it failed
                    console.log(err);
                });
            }//Tells the bot to join a specified channel, and saves it to the channelList in the DB
            else if (message.indexOf('!part') === 0){
                chat_client.part(messageParts[1]).then(function(){ //Left the channel properly, update channels and channelList key in DB
                    channels.splice(channels.indexOf(messageParts[1]), 1);
                    db.srem('channelList', messageParts[1]);
                    group_client.whisper(username,'The bot has left, or was not it, channel ' + messageParts[1]);
                }, function(err){ //Failed to leave the channel
                    console.log(err);
                });
            } //Tells the bot to leave a specified channel, and updates the channelList key in the DB
        }
        else {
            group_client.whisper(username, 'Sorry, you do not have admin control over this bot.');
        }
    }); //Checks if an admin command was sent and then responds accordingly

} //Helper function that makes sure all of the listeners get loaded after the bot clients have been initialized PUT ALL LISTENERS IN HERE

//Helper Functions

/**
 * isAdmin
 * @param username
 * @returns boolean
 * Checks if the given username is a member of the 'admins' set in the database
 */
function isAdmin(username){

    var res = db.sismember('admins', username, function(err, reply){
       return reply;
    });

    return res;
}
/**
 * setThreshold
 * @param threshold
 * Called after the bot receives a '!threshold #' whisper from an admin, sets the threshold in the database and changes the config stored in memory
 */
function setThreshold (threshold){
    db.hset('config', 'threshold', threshold);
    postThreshold = threshold;
}