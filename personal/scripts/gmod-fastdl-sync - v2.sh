#!/bin/sh

#To use this properly a few fields need to be edited, they are marked throughout the script

#Doing `find ./gmod_basedir/ -type f -print | cut -c3- | awk '{print "resource.AddFile(\""$0"\")"}'`>resources.lua will print the file names in the proper format for resources.lua
	

#Define used variables here to avoid null pointer exception
SERVER=NONE
FASTDL=NONE
GMROOT=NONE

while  [ $SERVER != "" ]; do

echo "Enter the name of the server's install directory relative to /home/servers/ or type quit to exit"
read SERVER
	#To make this script work on other servers you will need to edit these variables to point to the right place
	GMROOT="/home/servers/$SERVER/orangebox/garrysmod"
	FASTDL="/var/www/fastdl/gmod/$SERVER"	
	
if [ "$SERVER" = "quit" ]; then
	
	exit

elif [ -d $GMROOT ]; then
  
	echo "Making directories on FastDL server if they don't already exist."
		mkdir -p $FASTDL/models $FASTDL/materials $FASTDL/maps $FASTDL/cache $FASTDL/resources $FASTDL/sounds
	
	echo "Checking for existing bzipped map files"
		mv $FASTDL/maps/*.bz2 $GMROOT/maps
	
	echo "Compressing map files for $SERVER"
		bzip2 -k $GMROOT/maps/*.bsp

	echo "Moving compressed maps to $FASTDL"
		mv -u $GMROOT/maps/*.bz2 $FASTDL/maps/
	
	if [ -d $GMROOT/cache ]; then
		echo "Copying cache file  from $SERVER to $FASTDL/cache"
			cp -r -u $GMROOT/cache/* $FASTDL/cache/
	else
	fi
	
	if	[ "echo `ls $GMROOT/addons/*`" != "" ]; then
	#This is the exploratory phase where it should take the models, materials, and sounds from addons and put them into the fastdl
	
	#For some reason the addons.txt file is throwing an error, temporarily move it out of the addons folder to avoid this
		mkdir -p $GMROOT/temp
		mv $GMROOT/addons/addons.txt $GMROOT/tmp/addons.txt
		
		echo "Copying materials from installed addons to $FASTDL/materials"
			cp -r -u $GMROOT/addons/*/materials/* $FASTDL/materials
	
		echo "Copying models from installed addons to $FASTDL/models"
			cp -r -u $GMROOT/addons/*/models/* $FASTDL/models
		
		echo "Copying sounds from installed addons to $FASTDL/sounds"
			cp -r -u $GMROOT/addons/*/sounds/* $FASTDL/sounds
		
		echo "Copying resources from installed addons to $FASTDL/resources"
			cp -r -u $GMROOT/addons/*/resources/* $FASTDL/resources
			
	#Bring the addons.txt file back now that everything related to it has already happened.
		mv $GMROOT/tmp/addons.txt $GMROOT/addons/addons.txt

		#This part will generate the resoure file, do not uncomment until you figure out where to put the resource file and test other functions.		
		echo "Generating the resources.lua file"
			find $FASTDL/models $FASTDL/materials $FASTDL/sounds $FASTDL/resources -type f -print | cut -c3- | awk '{print "resource.AddSingleFile( \""$0"\" )"}'>$GMROOT/lua/autorun/server/resources.lua
		
		echo "The FastDL for $SERVER should now be up to date."
			sleep 2
			exit
	
	else
		
		echo "The FastDL for $SERVER should now be up to date."
			sleep 2
			exit
	
	fi
	
else
	
	echo "Please enter a valid install directory."

fi

done

