import maya.cmds as cmds


def reset(os):
    objects = os
    for object in objects:
        cmds.delete(object)
        objects.remove(object)
    return objects


def init_vis():

    everything_everywhere = []
    plane = cmds.polyPlane(
            w=1, h=1, sx=29, sy=39, ax=(0, 1, 0), cuv=2, ch=1)[0]
    cmds.scale(10,10,10, plane)
    everything_everywhere.append(plane)

    # TESTS
    print(everything_everywhere)
    everything_everywhere = reset(everything_everywhere)
    print(everything_everywhere)
