import json
import sys
from pprint import pprint


class TimeInterval(object):
    def __init__(self, common_words):
        self.messages = []
        self.users = {}
        self.unique_users = 0
        self.subscribers = 0
        self.words = common_words
        self.word_count = {}

        for w in self.words:
            self.word_count[w] = 0

    # Adds messages to the TimeInterval
    def add_message(self, message):
        self.messages.append(message)
        self.update_word_counts(message["message"])
        self.update_user_counts(message)
        # pprint(message)

    # Count the number of times each of the common words appears in the message and update the master count
    def update_word_counts(self, message):
        for word in message.split():
            if word in self.word_count:
                self.word_count[word] += 1

    def update_user_counts(self, message):
        username = message["username"]
        if username in self.users:
            self.users[username] += 1
        else:
            self.users[username] = 1
            self.unique_users += 1
            if message["sub"]:
                self.subscribers += 1

    # Prints the count of all common words
    def get_word_counts(self):
        for word in self.words:
            print(self.word_count[word])


def is_ignored(word):
    ignored_words = ["the", "be", "to", "of", "and", "a", "in", "that", "have", "i", "it", "for", "not", "on", "with",
                     "as", "you", "do", "at", "this", "but", "by", "from", "they", "we", "or", "an", "my", "there",
                     "their", "than", "then", "is", "was", "your", "are", "it's", "if", "has", "i'm", "its", "those",
                     "u"]

    if word in ignored_words:
        return True
    else:
        return False


# Returns a list of the 100 most common words
def get_common_words(data, num_words):
    word_list = {}
    result = []
    for m in data:
        message_content = m["message"]
        for w in message_content.split():
            word = w.lower()
            if not is_ignored(word):
                if word in word_list:
                    word_list[word] = word_list[word] + 1
                else:
                    word_list[word] = 1

    # Return only the num_words most common words
    count = num_words
    for word in sorted(word_list, key=word_list.get, reverse=True):
        if count > 0:
            # print(word, word_list[word])
            result.append(word)
            count -= 1
        else:
            return result


def get_time_intervals(file):
    if file is None:
        print("ERROR - What the fuck are you even doing")
        sys.exit()
    else:
        data_file = file

    # Load the data file into an array of objects
    with open(data_file) as df:
        data = json.load(df)

    common_words = get_common_words(data, 100)
    time_intervals = []
    time_intervals.append(TimeInterval(common_words))
    last_bin = 0

    for message in data:
        if message["bin"] != last_bin:
            # time_intervals[last_bin].get_word_counts()
            last_bin += 1
            time_intervals.append(TimeInterval(common_words))
        else:
            time_intervals[last_bin].add_message(message)

    return time_intervals


if __name__ == "__main__":
    get_time_intervals(None)
