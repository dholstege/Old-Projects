/**
 * Created by Davis on 2/21/2017.
 */

window.requestFileSystem = window.requestFileSystem || window.webkitRequestFileSystem;
var fs; //HTML filesystem

//HTML DOM objects
var encryptTabButton;
var decryptTabButton;
var settingsTabButton;
var encryptionTab;
var decryptionTab;
var settingsTab;
var encryptionFileEntry;
var decryptionFileEntry;
var encryptButton;
var decryptButton;
var outputArea;
var passwordEntry;
var passwordButton;

var password; //set once the passwordEntryButton has been clicked

function onInitFs(f){
    fs = f;
    console.log("DEBUG - Loaded a filesystem: " + fs.name);
}

function errorHandler(e) {
    console.log('DEBUG - Error: ' + e);
}

/*
 * This function should first set up a global reference to HTML elements
 * then add any listeners needed for those elements using addEventListener
 */
function setupElements(){
    console.log("DEBUG - Setting up listeners");
    encryptionFileEntry = document.querySelector('#encryptionFileEntry');
    //encryptionFileEntry.addEventListener('change', encryptionFileEntryAction, false);

    decryptionFileEntry = document.querySelector('#decryptionFileEntry');
    //TODO: event listener

    encryptButton = document.querySelector('#encryptButton');
    encryptButton.addEventListener('click', encryptButtonAction, false);

    decryptButton = document.querySelector('#decryptButton');
    decryptButton.addEventListener('click', decryptButtonAction, false);

    outputArea = document.querySelector('#outputArea');

    encryptionTab = document.querySelector('#encryptionTab');

    decryptionTab = document.querySelector('#decryptionTab');

    settingsTab = document.querySelector('#settingsTab');

    passwordEntry = document.querySelector('#passwordEntry');

    passwordButton = document.querySelector('#passwordButton');
    passwordButton.addEventListener('click', passwordButtonAction, false);

    encryptTabButton = document.querySelector('#encryptTabButton');
    encryptTabButton.addEventListener('click', function(){
        encryptionTab.style.display = 'block';
        decryptionTab.style.display = 'none';
        settingsTab.style.display = 'none';
    }, false);

    decryptTabButton = document.querySelector('#decryptTabButton');
    decryptTabButton.addEventListener('click', function(){
        decryptionTab.style.display = 'block';
        encryptionTab.style.display = 'none';
        settingsTab.style.display = 'none';
    }, false);

    settingsTabButton = document.querySelector('#settingsTabButton');
    settingsTabButton.addEventListener('click', function(){
        encryptionTab.style.display = 'none';
        decryptionTab.style.display = 'none';
        settingsTab.style.display = 'block';
    }, false);
}

//Clear anything that was in the output area then encrypt selected files
function encryptButtonAction(){
    document.querySelector('#encryptionOutputArea').innerHTML = '';
    importFile('encrypt');
}

//Clear anything that was in the output area then decrypt selected files
function decryptButtonAction(){
    document.querySelector('#decryptionOutputArea').innerHTML = '';
    importFile('decrypt');
}

function passwordButtonAction(){
    var passwordInput = passwordEntry.value;
    if (passwordInput.length < 6){
        displayError(1);
    }
    else{
        password = passwordInput;
        clearErrors();
    }
}

//displays the error with a given code, see switch for error codes
function displayError(errorCode){
    var errorMessage = document.querySelector('#errorMessage');
    switch (errorCode){
        case 1: //password length
            document.querySelector('#passwordLengthError').style.display = 'block';
            break;
        case 2: //no password set
            document.querySelector('#passwordNotSetError').style.display = 'block';
            break;
        case 3: //trying to decrypt something with wrong extension
            document.querySelector('#invalidFileExtensionError').style.display = 'block';
            break;
        case 4: //bad decryption password
            document.querySelector('#wrongPasswordError').style.display = 'block';
            break;
        default:
            break;
    }
}

//This will clear any errors displaying
function clearErrors(){
    var errorDisplays = document.querySelectorAll('.error');
    for (var i=0 ; i<errorDisplays.length ; i++){
        errorDisplays[i].style.display = 'none';
    }
}

/*
 * Imports a file into the sandbox filesystem
 */
function importFile(action){
    var inputFiles;

    if(!password){ //check if the password has been set, if not display error and return
        displayError(2);
        return;
    }
    else{ //password was set, clear errors just in case the error was displaying previously
        clearErrors();
    }

    //Determine which entry to import files from
    if (action === 'encrypt'){
        inputFiles = encryptionFileEntry.files;
    }
    else if (action === 'decrypt'){
        inputFiles = decryptionFileEntry.files;
    }

    //For each file in the input, import to filesystem and encrypt/decrypt as needed
    for (var i=0; i<inputFiles.length; i++){
        (function() { //This creates a closure so multiple file input works as expected
            var f = inputFiles[i];

            var fileName = f.name;

            //This is to make sure we aren't just returning the original input file data
            fs.root.getFile(fileName, {create: false}, function(fileEntry){
               fileEntry.remove(function(){
                   console.log("DEBUG - removed old file, if it existed");
               });
            });

            if (action === 'encrypt'){
                fileName = fileName+'.encrypted';
            }
            else if (action === 'decrypt' && f.name.endsWith('.encrypted')){
                fileName = fileName.slice(0,-10);
            }
            else{
                console.log("DEBUG - Error determining proper action on file: " + fileName);
                displayError(3);
                return; //this is return instead of break because we're in a closure
            }

            fs.root.getFile(fileName, {create: true}, function (fileEntry) {
                var reader = new FileReader();

                if (action === 'encrypt') reader.readAsArrayBuffer(f);
                else if (action === 'decrypt') reader.readAsText(f); //this should be a string

                reader.onload = function (e) {
                    //Create a file entry for what we just read on the HTML filesystem
                    fileEntry.createWriter(function (fileWriter) {

                        fileWriter.onwriteend = function (res) {
                            console.log("DEBUG - Finished writing " + fileName);
                            generateDownloadLink(fileEntry, action);
                        };

                        fileWriter.onerror = function (res) {
                            console.log("DEBUG - Error writing file: " + res);
                        };

                        if (action === 'encrypt'){
                            var data = new Uint8Array(reader.result); //this is an ArrayBuffer that magically works
                            var blob = new Blob([encryptUint8Array(data)], {type: f.type});
                        }
                        else if (action === 'decrypt'){
                            var data = reader.result; //should just be plaintext here
                            var blob = new Blob([decrypt(data)], {type: f.type});
                            if(blob.size === 4) return; //bad password

                        }
                        else {
                            console.log("DEBUG - Error invalid action type");
                        }
                        fileWriter.write(blob);

                    }, errorHandler);
                }


            }, errorHandler);
        })(); //This ugly line is part of the closure
    }

}


/*
 * This function generates a download link and adds it to the desired div in the window
 * Should be used after the fileEntry has been decrypted and remade
 * Takes a fileEntry to use as a data URI in the link
 */
function generateDownloadLink(fileEntry, action){

    var a = document.createElement('a');
    var br = document.createElement('br');
    a.setAttribute('href', fileEntry.toURL());
    a.setAttribute('download', fileEntry.name);
    a.innerHTML = fileEntry.name;

    if (action === 'encrypt'){
        document.querySelector('#encryptionOutputArea').appendChild(a);
        document.querySelector('#encryptionOutputArea').appendChild(br);
    }
    else if (action === 'decrypt'){
        document.querySelector('#decryptionOutputArea').appendChild(a);
        document.querySelector('#decryptionOutputArea').appendChild(br);
    }
    else {
        console.log("DEBUG - Error appending download link to output area");
    }

}

/*
 * Used internally to encrypt file
 */
function encryptUint8Array(input){
    var startTime = new Date().getTime();
    var tempArray = Array.prototype.slice.call(input); //this gets me a regular array that is easier to work with
    var plainText = tempArray.toString();
    var encrypted = CryptoJS.AES.encrypt(plainText, password);
    var endTime = new Date().getTime();
    console.log("INFO - Encrypted file in " + (endTime-startTime) + " milliseconds");
    return encrypted;
}


function decrypt(ciphertext) {
    /*
     * I know it looks like madness to convert anything into Utf8, but it makes sense I promise.
     * The original thing that was encrypted was a string representation of a Uint8 array, basically it was something
     * that looked like "123,456,789". After decrypting the ciphertext that was loaded from a file, what we get is
     * the hex representation of that string. To get the original file back we need to go from hex -> string (hence the Utf8)
     * -> Uint8Array -> blob.
     */
    var startTime = new Date().getTime();
    try {
        var decryptedString = CryptoJS.AES.decrypt(ciphertext, password).toString(CryptoJS.enc.Utf8);
    }
    catch(err){
        displayError(4);
        console.log("DEBUG - bad password used for decryption");
        return null;
    }
    var res = Uint8Array.from(decryptedString.split(','));
    var endTime = new Date().getTime();
    console.log("INFO - Decrypted file in " + (endTime-startTime) + " milliseconds");
    return res;
}

//This event listener triggers once the static content has been loaded on the page
document.addEventListener("DOMContentLoaded", function(event){
    window.requestFileSystem(window.TEMPORARY, 50*1024*1024, onInitFs, errorHandler);
    setupElements();
});
