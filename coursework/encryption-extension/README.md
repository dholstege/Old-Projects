# Chrome File Encryption Extension
Term project for CIS 433 Computer and Network Security

This browser extension allows portable encryption and decryption of personal files.
Encryption is AES, done using [crypto-js](code.google.com/p/crypto-js). Files are imported into the [HTML 5 FileSystem](https://developer.mozilla.org/en-US/docs/Web/API/FileSystem) before encryption, and returned as a browser download link.

## Setup
This project is provided as an unpacked Chrome extension. 

It can be added to a Chrome browser using the instructions [here](https://developer.chrome.com/extensions/getstarted)

```
1) Open the Extension Management page by navigating to chrome://extensions.
    The Extension Management page can also be opened by clicking on the Chrome menu, hovering over 
    More Tools then selecting Extensions.

2) Enable Developer Mode by clicking the toggle switch next to Developer mode.

3) Click the LOAD UNPACKED button and select the extension directory.
```

## Usage
Clicking the Extension icon should bring up a dialog allowing multiple files to be encrypted/decrypted
