#!/bin/bash

echo "Trying to find alignment in input file $1"

python norming.py < $1 > $2

python binning.py < $2 > $3

java -jar alignment < $3 > $4
