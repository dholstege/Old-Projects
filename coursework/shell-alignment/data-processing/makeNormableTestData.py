# Created 2016.11.30 from makeTestData.py 
# to try to create reverse-normed data 
# that can then be normed and used

import math
import sys
import random
import numpy
A = 1
B = 2
C = 3

def reversenorm(A, B, C, x_value, y_value):
    growth_factor = A*math.exp(B*x_value) + C
    return y_value*growth_factor


def generateTimeline(length, stddev):

    Timeline = []

    for i in range(length):
        Timeline.append(numpy.random.normal(loc=1,scale=stddev))

    return Timeline
    

def getShells(timeline, numShells, minLen, maxLen):

    shells=[]

    for i in range(numShells):
        shellLen = random.randint(minLen, maxLen)
        start = random.randint(0,len(timeline))
        end = start + shellLen
        
        for j in range(shellLen):
          shells.append(reversenorm(A, B, C, j, timeline[start+j])
    
    return shells


def main():

    params = sys.argv

    random.seed()

    if len(params) != 6:
        print "Invalid params"
        sys.exit(1)
    else:
        reads = int(params[1])
        minLen = int(params[2])
        maxLen = int(params[3])
        timelineLen = int(params[4])
        stddev = float(params[5])


    timeline = generateTimeline(timelineLen, stddev)
    shells = getShells(timeline, reads, minLen, maxLen)


    output = "Sample output file\n"

    for i in range(len(shells)):
        line = "sample" + str(i+1) + ","
        shellLen = len(shells[i])
        read = [shells[i][j] for j in range(shellLen)]
        line += ','.join(map(str,read))
        line += "\n"
        output += line

    print output[:-1]

main()
    
