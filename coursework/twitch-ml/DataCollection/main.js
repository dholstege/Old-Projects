/**
 * Created by Davis on 3/15/2017.
 */
var tmi = require('tmi.js');
var fs = require('fs');
var lists = require('./lists');

var options = {
    options: {
        debug: false
    },
    connection: {
        reconnect: true
    },
    identity: {
        username: /*USERNAME*/,
        password: /*OAUTH*/
    },
    channels: [/*CHANNELS*/]
};
var client;

var messageCache = {};
var sampleStore = [];

//Run the bot
init();

function init(){
    client = new tmi.client(options);
    client.connect();
    registerListeners();
}

function registerListeners(){

    //Message sent on a channel
    client.on('chat', function(channel, userstate, message, self){
        //Check that messages aren't sent by mods and aren't sent by the bot itself, then cache
        if (!userstate.mod && !self) {
            cacheMessage(channel, userstate, message);
        }
        //FIXME this is just for testing
        /*else {
            cacheMessage(channel, userstate, message);
        }*/
    });

    //Someone was banned on a channel (includes timeouts)
    client.on('ban', function(channel, username, reason){
        console.log("DEBUG - Someone got banned");
        if (messageCache[channel+username]){
            storeSample(messageCache[channel+username].concat(1)); //store the cached message as a positive example
            messageCache[channel+username] = null;
        }
    });

    //Someone was timed out on a channel
    client.on('timeout', function(channel, username, reason, duration){
        console.log("DEBUG - Someone got timed out");
        if (messageCache[channel+username]){
            storeSample(messageCache[channel+username].concat(1)); //store the cached message as a positive example
            messageCache[channel+username] = null;
        }
    });

}

/*
 * Take a message and add it to the cached messages list using channel+username as the key.
 * When a new message is added to the cache for a user, move the last message to the
 * long term store
 */
function cacheMessage(channel, userstate, message){
    var username = userstate.username;
    var messageLength = message.length;
    var messageWords = message.split(' ').length;
    var capitalsRatio = calculateCapsRatio(message);
    var containsProfanity = checkProfanity(message);
    var containsSlurs = checkSlurs(message);
    var containsGoogleWords = checkGoogleWords(message);
    var containsCensored = checkCensored(message);
    var containsLink = checkLinks(message);
    var containsObfuscatedLink = checkObfuscatedLinks(message);
    var containsCensoredLink = checkCensoredLink(message);
    var containsGlobalEmote = checkGlobalEmotes(message);
    var containsSubscriberEmote = checkSubscriberEmotes(message);
    var isSub = checkSubscriber(userstate);

    //Build a feature array based on all the features just calculated
    // var featureArray = [messageLength, messageWords, capitalsRatio, containsProfanity, containsSlurs, containsCensored, containsLink, containsObfuscatedLink, containsCensoredLink, containsGlobalEmote, containsSubscriberEmote, isSub];
    var featureArray = [messageLength, messageWords, capitalsRatio, containsGoogleWords, containsCensored, containsLink, containsObfuscatedLink, containsCensoredLink, containsGlobalEmote, containsSubscriberEmote, isSub];
    //If there was a cached message then it wasn't banned, move to store with label of 0
    if(messageCache[channel+username]){
        storeSample(messageCache[channel+username].concat(-1)); //add a 0 to the message because it wasn't banned
    }

    //Cache the new featureArray until another message arrives
    messageCache[channel+username] = featureArray;
}

/*
 * Add a sample to the sampleStore for printing to a log file
 */
function storeSample(sample){
    sampleStore.concat(sample);
    console.log("DEBUG - stored a sample vector: " + sample);

    //TODO print to data file
    fs.appendFile('data.txt', (sample + "\r\n"), function(err){
        if (err) console.log("DEBUG - Error writing to file: " + err);
    });
}

/*
 * Checks if a message contains profanity
 */
function checkProfanity(message){
    var swears = ["shit", "shitter", "shitters", "shitting", "fuck", "fucks", "fucked", "fucking", "fucker", "fuckers", "damn", "damned", "cunt", "cunts", "bitch", "bitches", "whore", "whores", "slut", "sluts", "dick", "dicks", "ass", "asshole", "assholes", "cock"];
    var count = 0;
    var messageParts = message.toLowerCase().split(' ');
    for (var i=0 ; i<messageParts.length ; i++){
        // if (swears.indexOf(messageParts[i]) > -1) return 1;
        if (swears.indexOf(messageParts[i]) > -1) count++;
    }
    return count;
}

/*
 * Checks if a message contains slurs
 */
function checkSlurs(message){
    var slurs = ["fag", "fags", "faggot", "faggots", "nigger", "niggers", "nigga", "niggas", "nig", "nigs", "spic", "spics", "cracker", "dyke", "gook", "gooks", "chink", "chinks", "towelhead", "towelheads", "towel head", "towel heads"];
    var count = 0;
    var messageParts = message.toLowerCase().split(' ');
    for (var i=0 ; i<messageParts.length ; i++){
        // if (slurs.indexOf(messageParts[i]) > -1) return 1;
        if (slurs.indexOf(messageParts[i]) > -1) count++;
    }
    return count;
}

/*
 * Checks if message contains a word in Google's wordlist
 */
function checkGoogleWords(message){
    var words = lists.googleList;
    var count = 0;
    var messageParts = message.toLowerCase().split(' ');
    for (var i=0 ; i<messageParts.length ; i++){
        // if (slurs.indexOf(messageParts[i]) > -1) return 1;
        if (words[messageParts[i]] === 1) count++;
    }
    return count;
}

/*
 * Checks if a message contains what was probably a censored word
 */
function checkCensored(message){
    var censored = ["***", "****", "*****", "******"];
    var count = 0;
    var messageParts = message.toLowerCase().split(' ');
    for (var i=0 ; i<messageParts.length ; i++){
        // if (slurs.indexOf(messageParts[i]) > -1) return 1;
        if (censored.indexOf(messageParts[i]) > -1) count++;
    }
    return count;
}

/*
 * Checks if a message contains a link
 */
function checkLinks(message){
    //The Twitch rule for links is just string.string without a space
    var result = message.match(/\w+\.\w+/);

    if (result) return result.length;
    else return 0;
}

/*
 * Checks if a message contains an obfuscated link
 */
function checkObfuscatedLinks(message){
    var shorteners = ["bit.ly", "goo.gl", "owl.ly", "deck.ly", "su.pr", "lnk.co", "fur.ly", "moourl.com", "bit.do", "t.co", "lnkd.in", "db.tt",
        "qr.ae", "adf.ly", "cur.lv", "tinyurl.com", "ity.im", "q.gs", "is.gd", "po.st", "bc.vc", "twitthis.com", "u.to",
        "j.mp", "buzurl.com", "cutt.us", "u.bb", "yourls.org", "x.co", "prettylinkpro.com", "scrnch.me", "filoops.info",
        "vzturl.com", "qr.net", "1url.com", "tweez.me", "v.gd", "tr.im", "link.zip.net", "tinyarrows.com"];

    var messageParts = message.toLowerCase().split(' ');
    for (var i=0 ; i<messageParts.length ; i++){
        if (shorteners.indexOf(messageParts[i]) > -1) return 1;
    }
    return 0;
}

/*
 * Checks if a message contains a deleted link
 */
function checkCensoredLink(message){
    if (message.indexOf("<deleted link>") > -1) return 1;
    else return 0;
}

/*
 * Checks if a global emote is used
 */
function checkGlobalEmotes(message){
    var emotes = lists.globalEmotesArray;
    var count = 0;
    var messageParts = message.split(' ');
    for (var i=0 ; i<messageParts.length ; i++){
        // if (emotes.indexOf(messageParts[i]) > -1) return 1;
        if (emotes.indexOf(messageParts[i]) > -1) count++;
    }
    return count;
}

/*
 * Checks if a global emote is used
 */
function checkSubscriberEmotes(message){
    var emotes = lists.subscriberEmotesArray;
    var count = 0;
    var messageParts = message.split(' ');
    for (var i=0 ; i<messageParts.length ; i++){
        // if (emotes.indexOf(messageParts[i]) > -1) return 1;
        if (emotes.indexOf(messageParts[i]) > -1) count++;
    }
    return count;
}

/*
 * Ratio of capital letters to non capital letters
 * Currently just checks which has more
 */
function calculateCapsRatio(message){
    var numCaps = message.replace(/[^A-Z]/g, "").length;
    var numLower = message.replace(/[A-Z]/g, "").length;

    if (isNaN(numCaps/numLower)){
        return 0
    }
    else {
        /*console.log(numCaps);
        console.log(numLower);
        console.log(Math.round((numCaps / numLower)*100)/100);
        return (Math.round((numCaps / numLower)*100)/100); //gets a ratio truncated to 2 decimal places*/
        if (numLower>=numCaps) return 0; //mostly or evenly lowercase
        else return 1;
    }
}

/*
 * Checks if the user is a subscriber
 */
function checkSubscriber(userstate){
    if (userstate.subscriber){
        return 1;
    }
    else {
        return 0;
    }
}
