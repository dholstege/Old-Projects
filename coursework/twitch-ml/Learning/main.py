import numpy
from sklearn import svm, preprocessing
from sklearn import neighbors


# This function takes an array of samples and removes the specified indices
# first parameter should be a list of indices to ignore
def ignore_features(feature_indices, data):
    result = numpy.delete(data, feature_indices, 1)
    return result


def learn_svc(input_data):
    print("DEBUG - Learning SVC model")
    x = []  # the samples, without their classifiers
    y = []  # the classifiers of the samples

    for sample in input_data:
        x.append(sample[0:-1])
        y.append(sample[-1])

    clf = svm.SVC(C=.25, kernel='rbf', degree=4, gamma='auto', coef0=0.0, shrinking=True, probability=False,
                  tol=0.01, class_weight={1: 2}, max_iter=-1, decision_function_shape='ovo', cache_size=1000)
    clf.fit(x, y)

    return clf


def learn_knn(input_data):
    print("DEBUG - Learning K-NN model")
    x = []  # the samples, without their classifiers
    y = []  # the classifiers of the samples

    for sample in input_data:
        x.append(sample[0:-1])
        y.append(sample[-1])

    clf = neighbors.KNeighborsClassifier(n_neighbors=5, weights='distance', algorithm='auto', leaf_size=30, p=2,
                                         metric='minkowski', metric_params=None, n_jobs=1)
    clf.fit(x, y)

    return clf


def test_model(model, test_data):
    print("DEBUG - Testing data")
    x = []  # the samples, without their classifiers
    y = []  # the classifiers of the samples

    for sample in test_data:
        x.append(sample[0:-1])
        y.append(sample[-1])

    predictions = model.predict(x)

    right = 0
    wrong = 0
    positive_right = 0
    positive_wrong = 0
    negative_right = 0
    negative_wrong = 0

    for i in range(0, len(predictions)):
        if predictions[i] == y[i]:
            right += 1
            if y[i] == 1:
                positive_right += 1
            else:
                negative_right += 1
        else:
            wrong += 1
            if y[i] == 1:
                positive_wrong += 1
            else:
                negative_wrong += 1

    # print("DEBUG - Correct positive prediction: " + str(positive_right))
    # print("DEBUG - Incorrect positive predictions: " + str(positive_wrong))
    print("DEBUG - Positive accuracy: " + str(float(positive_right)/(positive_right+positive_wrong)*100))

    # print("DEBUG - Correct negative prediction: " + str(negative_right))
    # print("DEBUG - Incorrect negative predictions: " + str(negative_wrong))
    print("DEBUG - Negative accuracy: " + str(float(negative_right) / (negative_right + negative_wrong)*100))

    # print("DEBUG - Correct predictions: " + str(right))
    # print("DEBUG - Wrong predictions: " + str(wrong))
    print("DEBUG - Accuracy: " + str(float(right) / len(y) * 100))


def main():
    train_file = "train_data.csv"
    test_file = "test_data.csv"
    features_to_ignore = [0]

    train_data = numpy.loadtxt(train_file, delimiter=',')
    test_data = numpy.loadtxt(test_file, delimiter=',')

    train_data = ignore_features(features_to_ignore, train_data)
    test_data = ignore_features(features_to_ignore, test_data)

    scaler = preprocessing.MinMaxScaler()
    scaled_train_data = scaler.fit_transform(train_data)
    scaled_test_data = scaler.transform(test_data)

    # model = learn_svc(scaled_train_data)
    model = learn_knn(scaled_train_data)
    print(model)

    test_model(model, scaled_test_data)


if __name__ == "__main__":
    main()
